export class Attributes<T> {
  constructor(private data: T) {}

  // generic contraint on function <K extends keyof T>
  // limits the types K can be
  // value of K can only be one of the keys of T

  // return type annotation T[K]
  // Look at T and return value at key K

  get = <K extends keyof T>(key: K): T[K] => {
    return this.data[key];
  };

  set = (update: T): void => {
    Object.assign(this.data, update);
  };

  getAll(): T {
    return this.data;
  }
}
